class CovidModel {
  final int positif;
  final int dirawat;
  final int sembuh;
  final int meninggal;

  CovidModel(
      {required this.positif,
      required this.dirawat,
      required this.sembuh,
      required this.meninggal});

  factory CovidModel.fromJson(final json) {
    return CovidModel(
        positif: json['positif'],
        dirawat: json['dirawat'],
        sembuh: json['sembuh'],
        meninggal: json['meninggal']);
  }
}
