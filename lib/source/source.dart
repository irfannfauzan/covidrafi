import 'package:flutter/material.dart';

Color primaryBlack = Color(0xff202c3b);
Color defaultWhite = Colors.white;
Color secondColor = Color(0xFF4AB5E1);

class TextSource {
  static String greetings = "Welcome";
  static String avatarTrailing = "https://picsum.photos/200";
}
