import 'package:appscovid/source/source.dart';
import 'package:flutter/material.dart';

class GreetingsCard extends StatelessWidget {
  const GreetingsCard({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var hour = DateTime.now().hour;
    var mes = '';
    var imagess = '';
    if (hour < 11) {
      mes = 'Selamat Pagi';
      print('Pagi Berhasil');
      imagess = 'images/sunrise.png';
    } else if (hour >= 11 && (hour < 15)) {
      mes = 'Selamat Siang';
      print('Siang Berhasil');
      imagess = 'images/sun.png';
    } else if (hour >= 15 && (hour < 18)) {
      mes = 'Selamat Sore';
      print('Sore Berhasil');
      imagess = 'images/sunset.png';
    } else if (hour >= 18 && (hour < 23)) {
      mes = 'Selamat Malam';
      print('Malam Berhasil');
      imagess = 'images/malam.png';
    }
    return Padding(
      padding: const EdgeInsets.all(11),
      child: Container(
        height: 70,
        width: 380,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(15),
          color: Color(0xFF0081A0),
        ),
        child: ListTile(
          title: Text(
            mes + " ",
            style: TextStyle(
              color: defaultWhite,
            ),
          ),
          subtitle: Text("Rafii"),
          trailing: CircleAvatar(
            backgroundImage: AssetImage(imagess),
          ),
        ),
      ),
    );
  }
}
