import 'package:appscovid/page/homepage.dart';
import 'package:appscovid/source/source.dart';
import 'package:flutter/material.dart';

class GejalaCovid extends StatelessWidget {
  const GejalaCovid({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: ThemeData(primaryColor: secondColor),
      home: Scaffold(
        appBar: AppBar(
          title: Text("Gejala Covid-19"),
          leading: IconButton(
              onPressed: () {
                Navigator.push(
                    context, MaterialPageRoute(builder: (_) => HomePage()));
              },
              icon: Icon(Icons.arrow_back_sharp)),
        ),
        body: SingleChildScrollView(
          child: Column(
            children: [
              SizedBox(
                height: 3,
              ),
              GejalaWidget(
                images: 'images/g1.jpg',
                title:
                    'Gejala pertama yang dirasakan oleh penderita Covid-19 adalah sakit kepala. Lebih dari 10 persen pasien Covid-19 mengalami sakit kepala yang berlangsung lebih dari 72 jam. ',
              ),
              SizedBox(
                height: 20,
              ),
              GejalaWidget(
                  images: 'images/g2.jpg',
                  title:
                      'Gejala kedua adalah batuk. Batuk yang menjadi gejala Covid-19 adalah batuk kering yang terjadi terus menerus. Batuk kering terjadi setidaknya dalam waktu setengah hari.'),
              SizedBox(
                height: 20,
              ),
              GejalaWidget(
                  images: 'images/g3.jpg',
                  title:
                      'Gejala ketiga adalah sakit tenggorokan. Sakit tenggorokan adalah infeksi yang disebabkan oleh bakteri streptokokus. Terjadi ketika virus masuk kedalam tubuh.'),
              SizedBox(
                height: 20,
              ),
              GejalaWidget(
                images: 'images/g4.jpg',
                title:
                    'Gejala keempat adalah sesak napas. Sesak napas mungkin ringan dan tidak berlangsung lama.',
              ),
              SizedBox(
                height: 20,
              ),
              GejalaWidget(
                images: 'images/g5.jpg',
                title:
                    'Gejala kelima adalah demam. Demam biasanya muncul pada orang yang terinfeksi Covid-19, dengan suhu lebih dari 37,7 derajat celcius.',
              ),
              SizedBox(
                height: 20,
              ),
              GejalaWidget(
                images: 'images/g6.jpg',
                title:
                    'Gejala keenam adalah nyeri otot. Rasa nyeri yang muncul pada otot ini disebabkan adanya peradangan yang terjadi di dalam tubuh akibat infeksi virus Corona.',
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class GejalaWidget extends StatelessWidget {
  final String images;
  final String title;
  const GejalaWidget({
    Key? key,
    required this.images,
    required this.title,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListTile(
      leading: ClipRRect(
        borderRadius: BorderRadius.circular(8),
        child: Image.asset(
          images,
          height: 250,
          width: 55,
        ),
      ),
      title: Text(
        title,
      ),
    );
  }
}

class UpayaPenanggulangan extends StatelessWidget {
  const UpayaPenanggulangan({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: ThemeData(primaryColor: secondColor),
      home: Scaffold(
        appBar: AppBar(
          title: Text("Upaya Penanggulangan Covid-19"),
          leading: IconButton(
              onPressed: () {
                Navigator.push(
                    context, MaterialPageRoute(builder: (_) => HomePage()));
              },
              icon: Icon(Icons.arrow_back_sharp)),
        ),
        body: SingleChildScrollView(
          child: Column(
            children: [
              SizedBox(
                height: 3,
              ),
              UpayaWidget(
                images: 'images/u1.jpg',
                title:
                    'Masker berguna untuk melindungi diri dari risiko tertular penyakit sekaligus mencegah kita menularkan penyakit kepada orang lain.',
              ),
              SizedBox(
                height: 20,
              ),
              UpayaWidget(
                  images: 'images/u2.jpg',
                  title:
                      'Mencuci tangan dengan benar adalah cara paling sederhana namun efektif untuk mencegah penyebaran virus 2019-nCoV.'),
              SizedBox(
                height: 20,
              ),
              UpayaWidget(
                  images: 'images/u3.jpg',
                  title:
                      'Menjaga jarak aman antar warga merupakan salah satu cara yang dianjurkan untuk mencegah penyebaran virus corona.'),
              SizedBox(
                height: 20,
              ),
              UpayaWidget(
                images: 'images/u5.jpg',
                title:
                    'Hindari keluar rumah jika tidak berkepentingan, agar selalu terhindar dari penyebaran virus corona.',
              ),
              SizedBox(
                height: 20,
              ),
              UpayaWidget(
                images: 'images/u6.jpg',
                title:
                    'Selalu membersihkan dan menjaga ruangan dengan menggunakan disinfektan.',
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class UpayaWidget extends StatelessWidget {
  final String images;
  final String title;
  const UpayaWidget({
    Key? key,
    required this.images,
    required this.title,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListTile(
      leading: ClipRRect(
        borderRadius: BorderRadius.circular(8),
        child: Image.asset(
          images,
          height: 250,
          width: 55,
        ),
      ),
      title: Text(
        title,
      ),
    );
  }
}
