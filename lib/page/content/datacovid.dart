import 'package:appscovid/services/model/modelcovid.dart';
import 'package:appscovid/services/repository/services-covid19.dart';
import 'package:flutter/material.dart';

class MainCovid extends StatelessWidget {
  const MainCovid({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return FutureBuilder<CovidModel>(
      future: getDataCovid(),
      builder: (context, snapshot) {
        if (snapshot.hasData) {
          final res = snapshot.data;
          return GridView(
            shrinkWrap: true,
            physics: NeverScrollableScrollPhysics(),
            gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                crossAxisCount: 2, childAspectRatio: 2),
            children: <Widget>[
              Views(
                title: 'Positif',
                panelColor: Colors.red.shade500,
                textColor: Colors.black,
                count: res!.positif.toString(),
              ),
              Views(
                title: 'Dirawat',
                panelColor: Colors.blue.shade500,
                textColor: Colors.black,
                count: res.dirawat.toString(),
              ),
              Views(
                title: 'Sembuh',
                panelColor: Colors.green.shade500,
                textColor: Colors.black,
                count: res.sembuh.toString(),
              ),
              Views(
                title: 'Meninggal',
                panelColor: Colors.grey,
                textColor: Colors.black,
                count: res.meninggal.toString(),
              ),
            ],
          );
        }
        return CircularProgressIndicator();
      },
    );
  }
}

class Views extends StatelessWidget {
  final Color panelColor;
  final Color textColor;
  final String title;
  final String count;

  const Views(
      {Key? key,
      required this.panelColor,
      required this.textColor,
      required this.title,
      required this.count})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    return Container(
      margin: EdgeInsets.all(10),
      height: 80,
      width: width / 2,
      color: panelColor,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Text(
            title,
            style: TextStyle(
                fontWeight: FontWeight.bold, fontSize: 16, color: textColor),
          ),
          Text(count,
              style: TextStyle(
                  fontWeight: FontWeight.bold, fontSize: 16, color: textColor))
        ],
      ),
    );
  }
}
