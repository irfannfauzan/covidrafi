import 'package:appscovid/source/source.dart';
import 'package:appscovid/source/sourceposter.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';

class Poster extends StatelessWidget {
  const Poster({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: CarouselSlider.builder(
          itemCount: images1.length,
          itemBuilder: (context, index, realIndex) {
            final res = images1[index];
            final res2 = pesan[index];
            return buildImage(res, index, context, res2);
          },
          options: CarouselOptions(height: 280, autoPlay: true)),
    );
  }
}

Widget buildImage(String images, int index, BuildContext context, String res2) {
  return Container(
      margin: EdgeInsets.symmetric(horizontal: 5),
      color: defaultWhite,
      child: Column(
        children: <Widget>[
          GestureDetector(
              onTap: () {}, child: PosterImage(imageNews: images, title: res2))
        ],
      ));
}

class PosterImage extends StatelessWidget {
  final String imageNews;
  final String title;

  const PosterImage({
    Key? key,
    required this.imageNews,
    required this.title,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: <Widget>[
          Image(
            image: AssetImage(
              imageNews,
            ),
            fit: BoxFit.fill,
            height: 280,
            width: double.infinity,
          ),
        ],
      ),
    );
  }
}
