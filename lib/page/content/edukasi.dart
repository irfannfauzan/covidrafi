import 'package:appscovid/page/content/mainedukasi.dart';
import 'package:flutter/material.dart';

class EdukasiCovid extends StatelessWidget {
  const EdukasiCovid({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GridView(
      shrinkWrap: true,
      physics: NeverScrollableScrollPhysics(),
      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
          crossAxisCount: 2, childAspectRatio: 1),
      children: <Widget>[
        GestureDetector(
          onTap: () {
            print("tap");
          },
          child: GestureDetector(
            onTap: () {
              Navigator.push(
                  context, MaterialPageRoute(builder: (_) => GejalaCovid()));
            },
            child: Views(
                image: 'images/gejala.png',
                textColor: Colors.black,
                title: 'Gejala Covid-19'),
          ),
        ),
        GestureDetector(
          onTap: () {
            print("tap2");
            Navigator.push(context,
                MaterialPageRoute(builder: (_) => UpayaPenanggulangan()));
          },
          child: Views(
              image: 'images/corona.png',
              textColor: Colors.black,
              title: 'Upaya Penanggulangan'),
        ),
      ],
    );
  }
}

class Views extends StatelessWidget {
  final String image;
  final Color textColor;
  final String title;

  const Views({
    Key? key,
    required this.image,
    required this.textColor,
    required this.title,
  }) : super(key: key);

//   @override
//   Widget build(BuildContext context) {
//     double width = MediaQuery.of(context).size.width;
//     return Container(
//       margin: EdgeInsets.all(10),
//       height: 20,
//       width: width / 2,
//       decoration: BoxDecoration(border: Border.all(color: Colors.black)),
//       child: Column(
//         mainAxisAlignment: MainAxisAlignment.center,
//         children: <Widget>[
//           Image(
//             image: AssetImage(image),
//             height: 50,
//           ),
//           Text(
//             title,
//             style: TextStyle(
//                 fontWeight: FontWeight.bold, fontSize: 8, color: textColor),
//           ),
//         ],
//       ),
//     );
//   }
// }

  @override
  Widget build(BuildContext context) {
    return Card(
      margin: EdgeInsets.all(5),
      child: Column(
        children: <Widget>[
          Image(
            image: AssetImage(image),
            height: 150,
          ),
          Text(
            title,
            style: TextStyle(
                fontWeight: FontWeight.bold, fontSize: 15, color: textColor),
          ),
        ],
      ),
    );
  }
}
