import 'package:appscovid/page/content/datacovid.dart';
import 'package:appscovid/page/content/edukasi.dart';
import 'package:appscovid/page/content/greetings.dart';
import 'package:appscovid/page/content/poster.dart';
import 'package:appscovid/source/source.dart';
import 'package:flutter/material.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: ThemeData(primaryColor: secondColor),
      home: Scaffold(
        appBar: AppBar(
          title: Text("Covid-19"),
        ),
        body: SingleChildScrollView(
          scrollDirection: Axis.vertical,
          child: Column(
            children: <Widget>[
              GreetingsCard(),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Text(
                      "Poster Covid-19",
                      style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 20,
                          color: primaryBlack),
                    ),
                    SizedBox(
                      height: 10,
                    ),
                  ],
                ),
              ),
              Poster(),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Text(
                      "Data Covid-19",
                      style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 20,
                          color: primaryBlack),
                    ),
                  ],
                ),
              ),
              MainCovid(),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Text(
                      "Edukasi Covid-19",
                      style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 20,
                          color: primaryBlack),
                    ),
                  ],
                ),
              ),
              EdukasiCovid(),
            ],
          ),
        ),
      ),
    );
  }
}
